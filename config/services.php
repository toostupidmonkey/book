<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => 'sandbox7760497960d8455ab8151afbd9a61c80.mailgun.org',
        'secret' => 'key-54eedcc7f733ebd95562d9855d6008e1',
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'google' => [
        'client_id' => '735153724442-3oqd7m5fsvjql0tn6fj9r8a3b587blor.apps.googleusercontent.com',
        'client_secret' => '_12hqXCE3kgbLtiMe-yh733E', 
        'redirect' => env('GOOGLE_REDIRECT', 'http://localhost:8000/auth/google/callback'),
    ],

];
