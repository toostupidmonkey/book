<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});

Auth::routes();
Route::post('book-it', [
    'uses' => 'UserController@bookIt',
    'middleware' => ['web', 'auth']
]);
Route::get('auth/google', 'Auth\LoginController@redirectToProviderGoogle')->name('google');
Route::get('auth/google/callback', 'Auth\LoginController@handleProviderCallbackGoogle');
Route::get('confirm/{key}', 'UserController@confirm');
Route::get('/home', 'HomeController@index')->name('home');
