<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'google_avatar', 'google_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function checkRoomStatus($data)
    {
        $user = $this->whereBetween('arrive_date', array($data['arrive'], $data['departure']))->orWhere(function ($query) use ($data) {
            $query->whereBetween('departure_date', array($data['arrive'], $data['departure']));
        })->where('tenant', '1')->get();
        return $user->isEmpty();
    }
    public function getDateCurrentTenant()
    {
        return $this->select('arrive_date', 'departure_date')->where('tenant', '1')->get();
    }
}
