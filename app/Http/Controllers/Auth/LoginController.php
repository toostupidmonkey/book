<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\User;
use Auth;
use Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function redirectToProviderGoogle()
    {
        return Socialite::driver('google')->redirect();
    }
    public function handleProviderCallbackGoogle()
    {
        try {
            $user = Socialite::driver('google')->user();
        } catch (Exception $e) {
            return Redirect::to('auth/google');
        }

        $authUser = $this->findOrCreateUserGoogle($user);

        Auth::login($authUser, true);

        return Redirect::to('/home');
    }
    private function findOrCreateUserGoogle($User)
    {
        if ($authUser = User::Where('google_id', $User->id)->first()) {
            return $authUser;
        } 
        else if ($authUser = User::Where('email', $User->email)->first()){
            if($authUser->google_id != $User->id) {
                $authUser->google_id = $User->id;
                $authUser->save();   
            }
            return $authUser;
        }
        return User::create([
            'name' => $User->name,
            'email' => $User->email,
            'google_id' => $User->id,
            'password' => encrypt(str_random(8)),
        ]);
    }
    
}
