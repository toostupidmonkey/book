<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BookItRequest;
use App\User;
use Auth;
use Mail;

class UserController extends Controller
{
    public function bookIt(BookItRequest $request)
    {
        $data = $request->all();
        $user = new User();
        $user_id =  Auth::user()->id;
        $check = $user->checkRoomStatus($data);
        if($check) {
            $key = str_random(24);
            $user = $user->find($user_id);
            $user->arrive_date = $data['arrive'];
            $user->confirm_key = $key;
            $user->departure_date = $data['departure'];
            $res = $user->save();
                Mail::send('emails.confirm', ['url' => 'http://'.$request->getHttpHost().'/confirm/'.$key], function ($m) use ($key) {
                $m->from('booking@app.com', 'Confirm your order');
                $m->to(Auth::user()->email, Auth::user()->name)->subject('Confirm your order!');
            });
            if($res) {
                return response()->json([
                    'data' => 'Check your mail and confirm order'
                ], 200);
            }
            return response()->json([
                'data' => 'Ooopps we got errors while save'
            ], 500);
        }
        return response()->json([
            'data' => $user->getDateCurrentTenant()
        ]);
    }
    public function confirm(Request $request, $key)
    {
        $user = new User();
        $tenant = $user->select('id')->where('confirm_key', $key)->first();
        if(!isset($tenant->id)) {
            return 'Inviled user';
        }
        $user = $user->find($tenant->id);
        $user->tenant = '1';
        $res = $user->save();
        if($res) {
            return 'Your order confirmed';
        } else {
            return 'Error while save contact with us to resolve this problem';
        }
    }
}
